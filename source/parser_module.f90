!! Taken from PaPIM code

!> @brief This module is used to parse character string, by first splitting
!! them into "fields" [split_line()], and then interpreting them
!! [load_values()]. The Developers using this interface should not know
!! about the internal of the module, unless a new pattern of "load_values()"
!! is needed. An auxiliary function to convert a string into uppercase is
!! provided, so that keywords can be interpreted independent of the case.

MODULE parser_module
  
  PRIVATE
  PUBLIC :: split_line, load_values, PRL_abort
!  PUBLIC :: split_line, to_upper_case, to_lower_case, load_values
  
  !> @brief load_values() performs the interpretation of the fields on the
  !! line. Automatic checking of the input fields is performed. The Developer
  !! only needs to provide the output from split_line() and the variables
  !! that shall hold the values found on the line. Two general examples:
  !! CALL load_values(nfields, fields, field_types, keyword, variable)
  !! to interpret a line of type "<KEYWORD> <value>", and
  !! CALL load_values(nfields, fields, field_types, variable1, variable2)
  !! to interpret a line of type "<value1> <value2>".

  INTERFACE load_values
     MODULE PROCEDURE load_values_1string_1integer
     MODULE PROCEDURE load_values_1string_1real
     MODULE PROCEDURE load_values_1string_1logical
     MODULE PROCEDURE load_values_1string_nreal
     MODULE PROCEDURE load_values_2strings
     MODULE PROCEDURE load_values_2strings_1real
     MODULE PROCEDURE load_values_1string_2reals
     MODULE PROCEDURE load_values_2strings_1integer_1real
     MODULE PROCEDURE load_values_1string_1stringarray
     MODULE PROCEDURE load_values_1string_1integer_1real
     MODULE PROCEDURE load_values_1string_1integer_2real
  END INTERFACE load_values
  
CONTAINS
  
  !============================================================================
  
  SUBROUTINE split_line(inputline, nfields, fields, field_types, &
       separator)
    
    !> @brief This routine takes a line and splits it into fields, using
    !! either an optional separator character or by default a whitespace
    !! character: The results are the number of fields found, their values
    !! and their types. Note on the types: They are generalised, so that
    !! for example '5' (without the quotation marks; if they are provided,
    !! the type is automatically a "STRING") is interpreted to be an
    !! integer number, but when checking the value, it could also be a real
    !! number, or even a string, without any numerical meaning.

    IMPLICIT NONE
    
    ! Arguments
    CHARACTER (LEN = *), INTENT (IN) :: inputline
    INTEGER, INTENT (OUT) :: nfields
    CHARACTER (LEN = *), DIMENSION (:), INTENT (OUT) :: fields
    CHARACTER (LEN = *), DIMENSION (:), INTENT (OUT) :: field_types
    CHARACTER, OPTIONAL, INTENT (IN) :: separator
    
    ! Local
    CHARACTER(*), PARAMETER :: separators=" ,="//char(9), comment_chars="#!"
    CHARACTER :: separator_in_use
    INTEGER :: i, nth_character_argument, field, error_code
    LOGICAL :: in_argument
    LOGICAL :: in_string_single, in_string_double
    
    INTEGER :: test_integer
    REAL :: test_real
    
    !--------------------------------------------------------------------------
    
    IF (PRESENT (separator)) THEN
       separator_in_use = separator
    ELSE
       separator_in_use = " "
    END IF
    
    fields(:) = ""
    field_types(:) = "NONE"
    
    nfields = 0
    nth_character_argument = 0
    in_string_single = .FALSE.
    in_string_double = .FALSE.
    in_argument = .FALSE.
    DO i = 1, LEN(TRIM(inputline))
       IF (inputline(i:i) == "'") THEN
          IF (in_string_double) THEN
             nth_character_argument = nth_character_argument + 1
             fields(nfields)(nth_character_argument:nth_character_argument) &
                  = inputline(i:i)
          ELSE
             IF (in_string_single) THEN
                ! End string
                in_string_single = .FALSE.
             ELSE
                ! Start string
                in_string_single = .TRUE.
                nfields = nfields + 1
                IF (nfields > SIZE(fields)) THEN
                   WRITE(6,*) "Too many arguments"
                   ! CALL error(...)
                END IF
                field_types(nfields) = "STRING"
                nth_character_argument = 0
             END IF
             !nth_character_argument = nth_character_argument + 1
             !fields(nfields)(nth_character_argument:nth_character_argument) &
             !     = inputline(i:i)
          END IF
       ELSE IF (inputline(i:i) == '"') THEN
          IF (in_string_single) THEN
             nth_character_argument = nth_character_argument + 1
             fields(nfields)(nth_character_argument:nth_character_argument) &
                  = inputline(i:i)
          ELSE
             IF (in_string_double) THEN
                ! End string
                in_string_double = .FALSE.
             ELSE
                ! Start string
                in_string_double = .TRUE.
                nfields = nfields + 1
                IF (nfields > SIZE(fields)) THEN
                   WRITE(6,*) "Too many arguments"
                   ! CALL error(...)
                END IF
                field_types(nfields) = "STRING"
                nth_character_argument = 0
             END IF
             !nth_character_argument = nth_character_argument + 1
             !fields(nfields)(nth_character_argument:nth_character_argument) &
             !     = inputline(i:i)
          END IF
       ELSE IF (in_string_single .OR. in_string_double) THEN
          nth_character_argument = nth_character_argument + 1
          fields(nfields)(nth_character_argument:nth_character_argument) &
               = inputline(i:i)
       ELSE IF (INDEX(separators,inputline(i:i))/=0) THEN
          IF (in_argument) THEN
             in_argument = .FALSE.
          END IF
       ELSE
          IF ( INDEX(comment_chars,inputline(i:i))/=0 .AND. .NOT. in_argument) THEN
             EXIT
          END IF
          IF (.NOT. in_argument) THEN
             nfields = nfields + 1
             IF (nfields > SIZE(fields)) THEN
                WRITE(6,*) "Too many arguments"
                ! CALL error(...)
             END IF
             nth_character_argument = 0
             in_argument = .TRUE.
          END IF
          nth_character_argument = nth_character_argument + 1
          fields(nfields)(nth_character_argument:nth_character_argument) &
               = inputline(i:i)
       END IF
    END DO
    
    IF (in_string_single .OR. in_string_double) THEN
       write(6,*) "String not closed"
       ! CALL error(...)
    END IF
    
    ! #1 Test for real
    ! #2 Test for integer
    ! #3 Test for logical
    ! #4 else: string
    DO field = 1, nfields
       IF (field_types(field) /= "NONE") CYCLE
       READ(UNIT = fields(field), FMT = *, IOSTAT = error_code) test_integer
       IF (error_code == 0) THEN
          field_types(field) = "INTEGER"
          CYCLE
       END IF
       READ(UNIT = fields(field), FMT = *, IOSTAT = error_code) test_real
       IF (error_code == 0) THEN
          field_types(field) = "REAL"
          CYCLE
       END IF
       IF (to_upper_case(fields(field)) == "TRUE" .OR. &
            to_upper_case(fields(field)) == "YES" .OR. &
            to_upper_case(fields(field)) == "NO" .OR. &
            to_upper_case(fields(field)) == "FALSE") THEN
!            to_upper_case(fields(field)) == "FALSE") THEN
          field_types(field) = "LOGICAL"
          CYCLE
       END IF
       field_types(field) = "STRING"
    END DO
    
  END SUBROUTINE split_line
  
  !============================================================================
  
  PURE FUNCTION to_upper (str) RESULT (string)
    
    !> @brief Returns a string in upper case
    
    IMPLICIT NONE
    
    CHARACTER (LEN = *), INTENT (IN) :: str
    CHARACTER (LEN(str))             :: string
    
    INTEGER :: ic, i
    
    CHARACTER(26), PARAMETER :: cap = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    CHARACTER(26), PARAMETER :: low = 'abcdefghijklmnopqrstuvwxyz'
    
    !   Capitalize each letter if it is lowercase
    string = str
    DO i = 1, LEN_TRIM(str)
       ic = INDEX(low, str(i:i))
       IF (ic > 0) string(i:i) = cap(ic:ic)
    END DO
    
  END FUNCTION to_upper

  !============================================================================

  PURE FUNCTION to_upper_case( str ) RESULT( string )
!> @brief Changes a string to upper case

    IMPLICIT NONE

    CHARACTER(LEN = *), INTENT(IN) :: str
    CHARACTER(LEN = LEN(str))      :: string

!! Localvariables
    INTEGER :: ic
    INTEGER :: i
    CHARACTER(26), PARAMETER :: cap = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    CHARACTER(26), PARAMETER :: low = 'abcdefghijklmnopqrstuvwxyz'

!! Capitalize each letter if it is lowecase
    string = str
    DO i = 1, LEN_TRIM(str)
       ic = INDEX(low, str(i:i))
       IF (ic > 0) string(i:i) = cap(ic:ic)
    END DO

  END FUNCTION to_upper_case

  !============================================================================

  PURE FUNCTION to_lower_case( str ) RESULT( string )

!> @brief Changes a string to lower case

    IMPLICIT NONE

    CHARACTER(LEN = *), INTENT(IN) :: str
    CHARACTER(LEN = LEN(str))      :: string

!! Local variables
    INTEGER :: ic
    INTEGER :: i
    CHARACTER(26), PARAMETER :: cap = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    CHARACTER(26), PARAMETER :: low = 'abcdefghijklmnopqrstuvwxyz'

!! Lower each letter if it is a capital letter
    string = str
    DO i = 1, LEN_TRIM(str)
       ic = INDEX(cap, str(i:i))
       IF (ic > 0) string(i:i) = low(ic:ic)
    END DO

  END FUNCTION to_lower_case
  
  !============================================================================
  
  SUBROUTINE PRL_abort(message)
    IMPLICIT NONE
    character(*), INTENT(in) :: message

    write(0,*) message
    stop

  END SUBROUTINE PRL_abort

  !============================================================================

  SUBROUTINE load_values_1string_1integer(nfields, fields, field_types, &
       string_arg, integer_arg)
    
    !> @brief An example of the interpreting routines of the "fields":
    !! The number and data types are checked.
    
    !USE prl, ONLY : PRL_abort
    
    IMPLICIT NONE
    
    ! Arguments
    INTEGER, INTENT (IN) :: nfields
    CHARACTER (LEN = *), DIMENSION (:), INTENT (IN) :: fields
    CHARACTER (LEN = *), DIMENSION (:), INTENT (IN) :: field_types
    CHARACTER (LEN = *), INTENT (OUT) :: string_arg
    INTEGER, INTENT (OUT) :: integer_arg
    
    IF (nfields /= 2) THEN
       CALL PRL_abort("load_values_1string_1integer(): Expected two arguments")
    END IF
    IF (field_types(1) /= "LOGICAL" .AND. field_types(1) /= "STRING" .AND. &
         field_types(1) /= "INTEGER" .AND. field_types(1) /= "REAL") THEN
       CALL PRL_abort("load_values_1string_1integer(): &
            &Expected argument type STRING, &
            &obtained " // TRIM(field_types(1)))
    END IF
    IF (field_types(2) /= "INTEGER") THEN
       CALL PRL_abort("load_values_1string_1integer(): &
            &Expected argument type INTEGER, &
            &obtained " // TRIM(field_types(2)))
    END IF
    
    string_arg = TRIM(fields(1))
    READ(UNIT = fields(2), FMT = *) integer_arg
    
  END SUBROUTINE load_values_1string_1integer
  
  !============================================================================
  
  SUBROUTINE load_values_1string_1logical(nfields, fields, field_types, &
       string_arg, logical_arg)
    
    !USE prl, ONLY : PRL_abort
    
    IMPLICIT NONE
    
    ! Arguments
    INTEGER, INTENT (IN) :: nfields
    CHARACTER (LEN = *), DIMENSION (:), INTENT (IN) :: fields
    CHARACTER (LEN = *), DIMENSION (:), INTENT (IN) :: field_types
    CHARACTER (LEN = *), INTENT (OUT) :: string_arg
    LOGICAL, INTENT (OUT) :: logical_arg
    
    IF (nfields /= 2) THEN
       CALL PRL_abort("load_values_1string_1logical(): Expected two arguments")
    END IF
    IF (field_types(1) /= "LOGICAL" .AND. field_types(1) /= "STRING" .AND. &
         field_types(1) /= "INTEGER" .AND. field_types(1) /= "REAL") THEN
       CALL PRL_abort("load_values_1string_1logical(): &
            &Expected argument type STRING, &
            &obtained " // TRIM(field_types(1)))
    END IF
    IF (field_types(2) /= "LOGICAL") THEN
       CALL PRL_abort("load_values_1string_1logical(): &
            &Expected argument type LOGICAL, &
            &obtained " // TRIM(field_types(1)))
    END IF
    
    string_arg = TRIM(fields(1))
    IF (to_upper_case(fields(2)) == "TRUE" .OR. &
        to_upper_case(fields(2)) == "YES") THEN
       logical_arg = .TRUE.
    ELSE IF (to_upper_case(fields(2)) == "FALSE" .OR. &
             to_upper_case(fields(2)) == "NO") THEN
       logical_arg = .FALSE.
    ELSE
       CALL PRL_abort("load_values_1string_1logical(), &
            &unknown value for a logical:" &
            // TRIM(fields(2)))
    END IF
    
  END SUBROUTINE load_values_1string_1logical

  !============================================================================

  SUBROUTINE load_values_1string_nreal(nfields, fields, field_types, &
       string_arg, real_arg)

    USE kinds, ONLY: dp
!    USE prl, ONLY: PRL_abort

    IMPLICIT NONE

    ! Arguments
    INTEGER, INTENT( IN ) :: nfields
    CHARACTER( LEN=* ), DIMENSION(:), INTENT( IN ) :: fields
    CHARACTER( LEN=* ), DIMENSION(:), INTENT( IN ) :: field_types
    CHARACTER( LEN=* ), INTENT( OUT ) :: string_arg
    REAL( dp ), DIMENSION(nfields-1), INTENT( OUT ) :: real_arg

    !Local
    INTEGER :: i_index

    IF( field_types(1) /= "STRING" ) THEN
       CALL PRL_abort( "load_values_1string_nreal: &
            &Expected argument type STRING, &
            &obtained "//TRIM( field_types(1) ) )
    END IF
    DO i_index = 2, nfields
        IF( field_types(i_index) /= "REAL" .AND. &
           &field_types(i_index) /= "INTEGER" ) THEN
            CALL PRL_abort( "load_values_1string_nreal: &
                &Expected argument type REAL or INTEGER, &
                &obtained "//TRIM( field_types(i_index) ) )
        END IF
    END DO

    string_arg = TRIM( fields(1) )
    READ( UNIT=fields(2:nfields), FMT=* ) real_arg

  END SUBROUTINE load_values_1string_nreal
 
  !============================================================================
  
  SUBROUTINE load_values_2strings(nfields, fields, field_types, &
       string_arg1, string_arg2)
    
    !USE prl, ONLY : PRL_abort
    
    IMPLICIT NONE
    
    ! Arguments
    INTEGER, INTENT (IN) :: nfields
    CHARACTER (LEN = *), DIMENSION (:), INTENT (IN) :: fields
    CHARACTER (LEN = *), DIMENSION (:), INTENT (IN) :: field_types
    CHARACTER (LEN = *), INTENT (OUT) :: string_arg1
    CHARACTER (LEN = *), INTENT (OUT) :: string_arg2
    
    IF (nfields /= 2) THEN
       CALL PRL_abort("load_values_2strings(): Expected two arguments")
    END IF
    IF (field_types(1) /= "LOGICAL" .AND. field_types(1) /= "STRING" .AND. &
         field_types(1) /= "INTEGER" .AND. field_types(1) /= "REAL") THEN
       CALL PRL_abort("load_values_2strings(): &
            &Expected argument type STRING, &
            &obtained " // TRIM(field_types(1)))
    END IF
    IF (field_types(2) /= "LOGICAL" .AND. field_types(2) /= "STRING" .AND. &
         field_types(2) /= "INTEGER" .AND. field_types(2) /= "REAL") THEN
       CALL PRL_abort("load_values_2strings(): &
            &Expected argument type STRING, &
            &obtained " // TRIM(field_types(2)))
    END IF
    
    string_arg1 = TRIM(fields(1))
    string_arg2 = TRIM(fields(2))
    
  END SUBROUTINE load_values_2strings
  
  !============================================================================
  
  SUBROUTINE load_values_1string_1real(nfields, fields, field_types, &
       string_arg, real_arg)
    
    USE kinds, ONLY : dp
    !USE prl, ONLY : PRL_abort
    
    IMPLICIT NONE
    
    ! Arguments
    INTEGER, INTENT (IN) :: nfields
    CHARACTER (LEN = *), DIMENSION (:), INTENT (IN) :: fields
    CHARACTER (LEN = *), DIMENSION (:), INTENT (IN) :: field_types
    CHARACTER (LEN = *), INTENT (OUT) :: string_arg
    REAL (KIND = dp), INTENT (OUT) :: real_arg
    
    IF (nfields /= 2) THEN
       CALL PRL_abort("load_values_1string_1real(): Expected one argument")
    END IF
    IF (field_types(1) /= "LOGICAL" .AND. field_types(1) /= "STRING" .AND. &
         field_types(1) /= "INTEGER" .AND. field_types(1) /= "REAL") THEN
       CALL PRL_abort("load_values_1strings_1real(): &
            &Expected argument type STRING, &
            &obtained " // TRIM(field_types(1)))
    END IF
    IF (field_types(2) /= "INTEGER" .AND. field_types(2) /= "REAL") THEN
       CALL PRL_abort("load_values_1string_1real(): Expected argument type REAL")
    END IF
    
    string_arg = TRIM(fields(1))
    READ(UNIT = fields(2), FMT = *) real_arg
    
  END SUBROUTINE load_values_1string_1real

  !============================================================================

  SUBROUTINE load_values_1string_1integer_1real(nfields, fields, field_types, &
       string_arg, integer_arg, real_arg)

    USE kinds, ONLY : dp
    !USE prl, ONLY : PRL_abort

    IMPLICIT NONE

    ! Arguments
    INTEGER, INTENT (IN) :: nfields
    CHARACTER (LEN = *), DIMENSION (:), INTENT (IN) :: fields
    CHARACTER (LEN = *), DIMENSION (:), INTENT (IN) :: field_types
    CHARACTER (LEN = *), INTENT (OUT) :: string_arg
    INTEGER, INTENT (OUT) :: integer_arg
    REAL (KIND = dp), INTENT (OUT) :: real_arg

    IF (nfields /= 3) THEN
       CALL PRL_abort("load_values_1string_1integer_1real(): Expected one argument")
    END IF
    IF (field_types(1) /= "LOGICAL" .AND. field_types(1) /= "STRING" .AND. &
         field_types(1) /= "INTEGER" .AND. field_types(1) /= "REAL") THEN
       CALL PRL_abort("load_values_1string_1integer_1real(): &
            &Expected argument type STRING, &
            &obtained " // TRIM(field_types(1)))
    END IF
    IF (field_types(2) /= "INTEGER") THEN
       CALL PRL_abort("load_values_1string_1integer_1real(): &
            &Expected argument type INTEGER, &
            &obtained " // TRIM(field_types(2)))
    END IF
    IF (field_types(3) /= "INTEGER" .AND. field_types(3) /= "REAL") THEN
       CALL PRL_abort("load_values_1string_1integer_1real(): &
            &Expected argument type REAL, &
            &obtained " // TRIM(field_types(3)))
    END IF

    string_arg = TRIM(fields(1))
    READ(UNIT = fields(2), FMT = *) integer_arg
    READ(UNIT = fields(3), FMT = *) real_arg

  END SUBROUTINE load_values_1string_1integer_1real

  !============================================================================

  SUBROUTINE load_values_1string_1integer_2real(nfields, fields, field_types, &
       string_arg, integer_arg, real_arg_1, real_arg_2)

    USE kinds, ONLY : dp
    !USE prl, ONLY : PRL_abort

    IMPLICIT NONE

    ! Arguments
    INTEGER, INTENT (IN) :: nfields
    CHARACTER (LEN = *), DIMENSION (:), INTENT (IN) :: fields
    CHARACTER (LEN = *), DIMENSION (:), INTENT (IN) :: field_types
    CHARACTER (LEN = *), INTENT (OUT) :: string_arg
    INTEGER, INTENT (OUT) :: integer_arg
    REAL (KIND = dp), INTENT (OUT) :: real_arg_1
    REAL (KIND = dp), INTENT (OUT) :: real_arg_2

    IF (nfields /= 4) THEN
       CALL PRL_abort("load_values_1string_1integer_2real(): Expected one argument")
    END IF
    IF (field_types(1) /= "LOGICAL" .AND. field_types(1) /= "STRING" .AND. &
         field_types(1) /= "INTEGER" .AND. field_types(1) /= "REAL") THEN
       CALL PRL_abort("load_values_1string_1integer_2real(): &
            &Expected argument type STRING, &
            &obtained " // TRIM(field_types(1)))
    END IF
    IF (field_types(2) /= "INTEGER") THEN
       CALL PRL_abort("load_values_1string_1integer_2real(): &
            &Expected argument type INTEGER, &
            &obtained " // TRIM(field_types(2)))
    END IF
    IF (field_types(3) /= "INTEGER" .AND. field_types(3) /= "REAL") THEN
       CALL PRL_abort("load_values_1string_1integer_2real(): &
            &Expected argument type REAL, &
            &obtained " // TRIM(field_types(3)))
    END IF
    IF (field_types(4) /= "INTEGER" .AND. field_types(4) /= "REAL") THEN
       CALL PRL_abort("load_values_1string_1integer_2real(): &
            &Expected argument type REAL, &
            &obtained " // TRIM(field_types(4)))
    END IF

    string_arg = TRIM(fields(1))
    READ(UNIT = fields(2), FMT = *) integer_arg
    READ(UNIT = fields(3), FMT = *) real_arg_1
    READ(UNIT = fields(4), FMT = *) real_arg_2

  END SUBROUTINE load_values_1string_1integer_2real
 
  !============================================================================
  
  SUBROUTINE load_values_1string_2reals(nfields, fields, field_types, &
       string_arg, real_arg1, real_arg2)
    
    USE kinds, ONLY : dp
    !USE prl, ONLY : PRL_abort
    
    IMPLICIT NONE
    
    ! Arguments
    INTEGER, INTENT (IN) :: nfields
    CHARACTER (LEN = *), DIMENSION (:), INTENT (IN) :: fields
    CHARACTER (LEN = *), DIMENSION (:), INTENT (IN) :: field_types
    CHARACTER (LEN = *), INTENT (OUT) :: string_arg
    REAL (KIND = dp), INTENT (OUT) :: real_arg1, real_arg2
    
    IF (nfields /= 3) THEN
       CALL PRL_abort("load_values_1string_2reals(): Expected three arguments")
    END IF
    IF (field_types(1) /= "LOGICAL" .AND. field_types(1) /= "STRING" .AND. &
         field_types(1) /= "INTEGER" .AND. field_types(1) /= "REAL") THEN
       CALL PRL_abort("load_values_2strings(): &
            &Expected argument type STRING, &
            &obtained " // TRIM(field_types(1)))
    END IF
    IF (field_types(2) /= "INTEGER" .AND. field_types(2) /= "REAL") THEN
       CALL PRL_abort("load_values_1string_2reals(): &
            &Expected argument type REAL, &
            &obtained " // TRIM(field_types(2)))
    END IF
    IF (field_types(3) /= "INTEGER" .AND. field_types(3) /= "REAL") THEN
       CALL PRL_abort("load_values_1string_2reals(): &
            &Expected argument type REAL, &
            &obtained " // TRIM(field_types(3)))
    END IF
    
    string_arg = TRIM(fields(1))
    READ(UNIT = fields(2), FMT = *) real_arg1
    READ(UNIT = fields(3), FMT = *) real_arg2
    
  END SUBROUTINE load_values_1string_2reals
  
  !============================================================================

  SUBROUTINE load_values_2strings_1real( &
       nfields, fields, field_types, &
       string_arg1, string_arg2, real_arg)

    USE kinds, ONLY : dp
    !USE prl, ONLY : PRL_abort

    IMPLICIT NONE

    ! Arguments
    INTEGER, INTENT (IN) :: nfields
    CHARACTER (LEN = *), DIMENSION (:), INTENT (IN) :: fields
    CHARACTER (LEN = *), DIMENSION (:), INTENT (IN) :: field_types
    CHARACTER (LEN = *), INTENT (OUT) :: string_arg1, string_arg2
    REAL (KIND = dp), INTENT (OUT) :: real_arg

    IF (nfields /= 3) THEN
       CALL PRL_abort("load_values_2strings_1real(): &
            &Expected three arguments")
    END IF
    IF (field_types(1) /= "LOGICAL" .AND. field_types(1) /= "STRING" .AND. &
         field_types(1) /= "INTEGER" .AND. field_types(1) /= "REAL") THEN
       CALL PRL_abort("load_values_2strings_1real(): &
            &Expected argument type STRING, position 1")
    END IF
    IF (field_types(2) /= "LOGICAL" .AND. field_types(2) /= "STRING" .AND. &
         field_types(2) /= "INTEGER" .AND. field_types(2) /= "REAL") THEN
       CALL PRL_abort("load_values_2strings_1real(): &
            &Expected argument type STRING, position 2")
    END IF
    IF (field_types(3) /= "INTEGER" .AND. field_types(3) /= "REAL") THEN
       CALL PRL_abort("load_values_2strings_1real(): &
            &Expected argument type REAL, position 3")
    END IF

    string_arg1 = TRIM(fields(1))
    string_arg2 = TRIM(fields(2))
    READ(UNIT = fields(3), FMT = *) real_arg

  END SUBROUTINE load_values_2strings_1real

  !============================================================================

  SUBROUTINE load_values_2strings_1integer_1real( &
       nfields, fields, field_types, &
       string_arg1, string_arg2, integer_arg, real_arg)
    
    USE kinds, ONLY : dp
    !USE prl, ONLY : PRL_abort
    
    IMPLICIT NONE
    
    ! Arguments
    INTEGER, INTENT (IN) :: nfields
    CHARACTER (LEN = *), DIMENSION (:), INTENT (IN) :: fields
    CHARACTER (LEN = *), DIMENSION (:), INTENT (IN) :: field_types
    CHARACTER (LEN = *), INTENT (OUT) :: string_arg1, string_arg2
    INTEGER, INTENT (OUT) :: integer_arg
    REAL (KIND = dp), INTENT (OUT) :: real_arg
    
    IF (nfields /= 4) THEN
       CALL PRL_abort("load_values_2strings_1integer_1real(): &
            &Expected four arguments")
    END IF
    IF (field_types(1) /= "LOGICAL" .AND. field_types(1) /= "STRING" .AND. &
         field_types(1) /= "INTEGER" .AND. field_types(1) /= "REAL") THEN
       CALL PRL_abort("load_values_2strings_1integer_1real(): &
            &Expected argument type STRING, position 1")
    END IF
    IF (field_types(2) /= "LOGICAL" .AND. field_types(2) /= "STRING" .AND. &
         field_types(2) /= "INTEGER" .AND. field_types(2) /= "REAL") THEN
       CALL PRL_abort("load_values_2strings_1integer_1real(): &
            &Expected argument type STRING, position 2")
    END IF
    IF (field_types(3) /= "INTEGER") THEN
       CALL PRL_abort("load_values_2strings_1integer_1real(): &
            &Expected argument type INTEGER, position 3")
    END IF
    IF (field_types(4) /= "INTEGER" .AND. field_types(4) /= "REAL") THEN
       CALL PRL_abort("load_values_2strings_1integer_1real(): &
            &Expected argument type REAL, position 4")
    END IF
    
    string_arg1 = TRIM(fields(1))
    string_arg2 = TRIM(fields(2))
    READ(UNIT = fields(3), FMT = *) integer_arg
    READ(UNIT = fields(4), FMT = *) real_arg
    
  END SUBROUTINE load_values_2strings_1integer_1real
  
  !============================================================================
  
  SUBROUTINE load_values_1string_1stringarray(nfields, fields, field_types, &
       string_arg, string_array)
    
    !USE prl, ONLY : PRL_abort
    
    IMPLICIT NONE
    
    ! Arguments
    INTEGER, INTENT (IN) :: nfields
    CHARACTER (LEN = *), DIMENSION (:), INTENT (IN) :: fields
    CHARACTER (LEN = *), DIMENSION (:), INTENT (IN) :: field_types
    CHARACTER (LEN = *), INTENT (OUT) :: string_arg
    CHARACTER (LEN = *), INTENT (OUT) :: string_array(:)
    
    ! Local
    INTEGER :: arg
    
    IF (nfields < 2) THEN
       write(6,*) nfields
       CALL PRL_abort("load_values_1string_1stringarray(): &
            &Expected at least two arguments")
    END IF
    IF (field_types(1) /= "LOGICAL" .AND. field_types(1) /= "STRING" .AND. &
         field_types(1) /= "INTEGER" .AND. field_types(1) /= "REAL") THEN
       CALL PRL_abort("load_values_1string_1stringarray(): &
            &Expected argument type STRING, &
            &obtained " // TRIM(field_types(1)))
    END IF
    IF (SIZE (string_array) < nfields - 1) THEN
       CALL PRL_abort("load_values_1string_1stringarray(): &
            &Too many values for a STRING array")
    END IF
    DO arg = 2, nfields
       IF (field_types(arg) /= "LOGICAL" .AND. &
            field_types(arg) /= "STRING" .AND. &
            field_types(arg) /= "INTEGER" .AND. field_types(arg) /= "REAL") THEN
          CALL PRL_abort("load_values_1string_1stringarray(): &
               &Expected argument type STRING, &
               &obtained " // TRIM(field_types(arg)))
       END IF
    END DO
    
    string_arg = TRIM(fields(1))
    DO arg = 2, nfields
       string_array(arg - 1) = TRIM(fields(arg))
    END DO
    
  END SUBROUTINE load_values_1string_1stringarray
  
  !============================================================================
  
END MODULE parser_module
