module potentials
	use glob_types
	IMPLICIT NONE

	abstract interface
		REAL(8) function func(X)
			IMPLICIT NONE
			REAL(8),INTENT(in)  :: X(:)
		end function func
	end interface
	abstract interface
		function dfunc(X) result(F)
			IMPLICIT NONE
			REAL(8),INTENT(in)  :: X(:)
			REAL(8)	:: F(size(X))
		end function dfunc
	end interface

	PROCEDURE (func), POINTER :: Pot => null ()
	PROCEDURE (dfunc), POINTER :: dPot => null ()

	TYPE(potential_parameters_type), SAVE :: pot_param
	
contains

	subroutine assign_potential(Ndim,pot_name)
		use atomic_units
		IMPLICIT NONE
		INTEGER, INTENT(in) :: Ndim
		CHARACTER(len=*), INTENT(in) :: pot_name
		
		!ASSIGN Pot AND dPot TO THE CHOSEN POTENTIAL (subprograms/potentials.f90)
		SELECT CASE(trim(pot_name))
		CASE("DM")
			if(Ndim/=2)	stop "Ndim must be equal to 2 for this potential. Stopping execution..."
			Pot=>Pot_DM
			dPot=>dPot_DM
		CASE("HO")
			Pot=>Pot_HO
			dPot=>dPot_HO
		CASE("QO")
			Pot=>Pot_QO
			dPot=>dPot_QO
		CASE("MO")
			Pot=>Pot_Mo
			dPot=>dPot_Mo
		CASE("CO")
			if(Ndim/=2)	stop "Ndim must be equal to 2 for this potential. Stopping execution..."
			Pot=>Pot_CO
			dPot=>dPot_CO
		CASE("DM1D")
			if(Ndim/=1)	stop "Ndim must be equal to 1 for this potential. Stopping execution..."
			Pot=>Pot_DM1D
			dPot=>dPot_DM1D
		CASE("CO2")
			if(Ndim/=3)	stop "Ndim must be equal to 3 for this potential. Stopping execution..."
            Pot=>Pot_CO2
            dPot=>dPot_CO2
		CASE DEFAULT
			write(0,*) "Error: '"//trim(pot_name)//"' is not a correct pot_name"
			stop
		END SELECT
		
	end subroutine assign_potential

	real(8) function Pot_DM(X)
		implicit none
		real(8),intent(in)  :: X(:)
		
		Pot_DM=pot_param%D*( &
					exp(-2*pot_param%alpha*(X(2)/2+X(1)-pot_param%d0)) &
					-2*exp(-pot_param%alpha*(X(2)/2+X(1)-pot_param%d0)) + 1 &
					+(pot_param%asym**2)*( &
						exp(-2*pot_param%alpha*(X(2)/2-X(1)-pot_param%d0)/pot_param%asym) &
						-2*exp(-pot_param%alpha*(X(2)/2-X(1)-pot_param%d0)/pot_param%asym) &
					) &
				) + pot_param%ADM*exp(-pot_param%BDM*X(2))-pot_param%CDM/(X(2)**6)
				
		
	end function Pot_DM

	function dPot_DM(X) result(F)
		implicit none
		real(8),intent(in)  :: X(:)
		real(8) :: F(size(X))

		F=0
		F(1)=pot_param%D*2*pot_param%alpha*( &
				-exp(-2*pot_param%alpha*(X(2)/2+X(1)-pot_param%d0)) &
				+ exp(-pot_param%alpha*(X(2)/2+X(1)-pot_param%d0)) &
				+ pot_param%asym*( &
					exp(-2*pot_param%alpha*(X(2)/2-X(1)-pot_param%d0)/pot_param%asym) &
					- exp(-pot_param%alpha*(X(2)/2-X(1)-pot_param%d0)/pot_param%asym) &
				) &
			  )

		F(2)=pot_param%D*pot_param%alpha*( &
				-exp(-2*pot_param%alpha*(X(2)/2+X(1)-pot_param%d0)) &
				+exp(-pot_param%alpha*(X(2)/2+X(1)-pot_param%d0)) &
				+pot_param%asym*( &
					-exp(-2*pot_param%alpha*(X(2)/2-X(1)-pot_param%d0)/pot_param%asym) &
					+exp(-pot_param%alpha*(X(2)/2-X(1)-pot_param%d0)/pot_param%asym) &
				) &
			) - pot_param%ADM*pot_param%BDM*exp(-pot_param%BDM*X(2))+6*pot_param%CDM/(X(2)**7)
		
	end function dPot_DM

	real(8) function Pot_DM1D(X)
		implicit none
		real(8),intent(in)  :: X(:)
		
		Pot_DM1D=pot_param%D*( &
					exp(-2*pot_param%alpha*(pot_param%dAB/2+X(1)-pot_param%d0)) &
					-2*exp(-pot_param%alpha*(pot_param%dAB/2+X(1)-pot_param%d0)) + 1 &
					+(pot_param%asym**2)*( &
						exp(-2*pot_param%alpha*(pot_param%dAB/2-X(1)-pot_param%d0)/pot_param%asym) &
						-2*exp(-pot_param%alpha*(pot_param%dAB/2-X(1)-pot_param%d0)/pot_param%asym) &
					) &
				) + pot_param%ADM*exp(-pot_param%BDM*pot_param%dAB)-pot_param%CDM/(pot_param%dAB**6)
		
	end function Pot_DM1D

	function dPot_DM1D(X) result(F)
		implicit none
		real(8),intent(in)  :: X(:)
		real(8) :: F(size(X))

		F=0
		F(1)=pot_param%D*2*pot_param%alpha*( &
				-exp(-2*pot_param%alpha*(pot_param%dAB/2+X(1)-pot_param%d0)) &
				+exp(-pot_param%alpha*(pot_param%dAB/2+X(1)-pot_param%d0)) &
				+pot_param%asym*( &
					exp(-2*pot_param%alpha*(pot_param%dAB/2-X(1)-pot_param%d0)/pot_param%asym) &
					-exp(-pot_param%alpha*(pot_param%dAB/2-X(1)-pot_param%d0)/pot_param%asym) &
				) &
			  )
		
	end function dPot_DM1D

	real(8) function Pot_HO(X)
		implicit none
		real(8),intent(in)  :: X(:)
		
		Pot_HO=0.5*sum(pot_param%mass_HO(:)*(pot_param%Omega0(:)*X(:))**2)
		
	end function Pot_HO

	function dPot_HO(X) result(F)
		implicit none
		real(8),intent(in)  :: X(:)
		real(8) :: F(size(X))

		F(:)=pot_param%mass_HO(:)*X(:)*pot_param%Omega0(:)**2
		
	end function dPot_HO

	real(8) function Pot_CO(X)
		implicit none
		real(8),intent(in)  :: X(:)
		
		Pot_CO=0.5*sum(pot_param%mass_HO(:)*(pot_param%Omega0(:)*X(:))**2) &
				+pot_param%c3*(X(1)-X(2))**3 &
				+pot_param%c4*(X(1)-X(2))**4
		
	end function Pot_CO

	function dPot_CO(X) result(F)
		implicit none
		real(8),intent(in)  :: X(:)
		real(8) :: F(size(X))
		real(8) :: dC

		F(:)=pot_param%mass_HO(:)*X(:)*pot_param%Omega0(:)**2

		dC=3*pot_param%c3*(X(1)-X(2))**2 &
			+ 4*pot_param%c4*(X(1)-X(2))**3

		F(1)=F(1)+dC
		F(2)=F(2)-dC
		
	end function dPot_CO
    
	real(8) function Pot_CO2(X)
		implicit none
		real(8),intent(in)  :: X(:)
		
		Pot_CO2=0.5_8*sum(pot_param%mass_HO(:)*(pot_param%Omega0(:)*X(:))**2) &
				+0.5_8*pot_param%c3*X(1)*(X(2)**2+X(3)**2) &
					*sqrt(pot_param%mass_HO(1))*pot_param%mass_HO(2)
		
	end function Pot_CO2

	function dPot_CO2(X) result(F)
		implicit none
		real(8),intent(in)  :: X(:)
		real(8) :: F(size(X))

		F(:)=pot_param%mass_HO(:)*X(:)*pot_param%Omega0(:)**2
		F(1)=F(1)+0.5_8*pot_param%c3*(X(2)**2+X(3)**2)*sqrt(pot_param%mass_HO(1))*pot_param%mass_HO(2)
		F(2)=F(2)+pot_param%c3*X(2)*X(1)*sqrt(pot_param%mass_HO(1))*pot_param%mass_HO(2)
        F(3)=F(3)+pot_param%c3*X(3)*X(1)*sqrt(pot_param%mass_HO(1))*pot_param%mass_HO(2)
		
	end function dPot_CO2

	real(8) function Pot_QO(X)
		implicit none
		real(8), intent(in) :: X(:)
		
		Pot_QO=pot_param%QO*sum(X(:)**4)
		
	end function Pot_QO

	function dPot_QO(X) result(F)
		implicit none
		real(8),intent(in) :: X(:)
		real(8) :: F(size(X))
		
		F(:)=4*pot_param%QO*X(:)**3
		
	end function dPot_QO

	real(8) function Pot_Mo(X)
		implicit none
		real(8), intent(in) :: X(:)
		
		if(sqrt(sum(X**2))<=pot_param%xmax) then
			Pot_Mo=sum(pot_param%D*(exp(-2*pot_param%alpha*X(:))-2*exp(-pot_param%alpha*X(:))+1))
		else
			Pot_Mo=sum(pot_param%D*(exp(-2*pot_param%alpha*X(:))-2*exp(-pot_param%alpha*X(:)) &
					+exp(10._8*(X(:)-pot_param%xmax))))
		endif
		
	end function Pot_Mo

	function dPot_Mo(X) result(F)
		implicit none
		real(8),intent(in) :: X(:)
		real(8) :: F(size(X))
		
		if(sqrt(sum(X**2))<=pot_param%xmax) then
			F(:)=pot_param%D*(-2*pot_param%alpha*exp(-2*pot_param%alpha*X(:)) &
					+2*pot_param%alpha*exp(-pot_param%alpha*X(:)))
		else
			F(:)=pot_param%D*(-2*pot_param%alpha*exp(-2*pot_param%alpha*X(:)) &
					+2*pot_param%alpha*exp(-pot_param%alpha*X(:))+10._8*exp(10._8*(X(:)-pot_param%xmax)))
		endif
		
	end function dPot_Mo
    
end module potentials