module misc_subroutines
	implicit none
	real(8), parameter :: eps=1E-9 ! MAX ERROR ON INVERSE MATRIX
	real(8), private, allocatable, save :: LO(:,:),V(:),WORK(:)
	integer, private, allocatable, save :: IPIV(:)
	
contains

	subroutine sym_mat_diag(mat,EVal,EVec,INFO) !USES LAPACK
		!DIAGONALIZE MATRIX mat WHICH MUST BE SYMMETRIC (CONTAINS AT LEAST THE LOWER TRIANGLE)
		implicit none
		real(8), intent(in) :: mat(:,:)
		integer, intent(out) :: INFO
		real(8), intent(inout) :: Eval(:),Evec(:,:)
		integer :: Ndim

		Ndim=size(mat,1)
		if(size(mat,2)/=Ndim) stop "mat must be a square matrix"
		if(.not. allocated(WORK)) allocate(LO(Ndim,Ndim),IPIV(Ndim),WORK(3*Ndim),V(Ndim))
		EVec=mat
		call DSYEV('V','L',Ndim,EVec,Ndim,EVal,WORK,3*Ndim,INFO)
	!	if(INFO/=0) stop "ERROR DURING MATRIX DIAGONALIZATION."

	end subroutine sym_mat_diag

	function sym_mat_exp(mat,INFO) result(matExp) !USES LAPACK
		!COMPUTE exp OF mat WHICH MUST BE SYMMETRIC (CONTAINS AT LEAST THE LOWER TRIANGLE)
		implicit none
		real(8), intent(in) :: mat(:,:)
		integer, intent(out) :: INFO
		real(8), allocatable :: matEXp(:,:)
		integer :: Ndim,i

		Ndim=size(mat,1)
		if(size(mat,2)/=Ndim) stop "mat must be a square matrix"
		allocate(matExp(Ndim,Ndim))
		if(.not. allocated(WORK)) allocate(LO(Ndim,Ndim),IPIV(Ndim),WORK(3*Ndim),V(Ndim))

		LO=mat
		call DSYEV('V','L',Ndim,LO,Ndim,V,WORK,3*Ndim,INFO)
		V=exp(V)
		do i=1,Ndim
			matExp(i,:)=V(i)*LO(i,:)
		enddo
		matExp=matmul(transpose(LO),matExp)

	end function sym_mat_exp
	
	function sym_mat_inverse(mat,INFO) result(matInv) !USES LAPACK
		!INVERSE MATRIX mat WHICH MUST BE SYMMETRIC (CONTAINS AT LEAST THE LOWER TRIANGLE)
		implicit none
		real(8), intent(in) :: mat(:,:)
		integer, intent(out) :: INFO
		real(8), allocatable :: matInv(:,:)
		integer :: Ndim,i
		
		Ndim=size(mat,1)
		if(size(mat,2)/=Ndim) stop "mat must be a square matrix"
		allocate(matInv(Ndim,Ndim))
		if(.not. allocated(WORK)) allocate(LO(Ndim,Ndim),IPIV(Ndim),WORK(3*Ndim),V(Ndim))
				
		matInv=0
		forall (i=1:Ndim) matInv(i,i)=1
		LO=mat			
		call DSYSV('L',Ndim,Ndim,LO,Ndim,IPIV,matInv,Ndim,WORK,3*Ndim,INFO)
	!	if(INFO/=0) then
	!		write(0,*) "ERROR DURING MATRIX INVERSION. ERROR", INFO
	!		do i=1,Ndim
	!			write(0,*) mat(i,:)
	!		enddo
	!		write(0,*) "det=", mat(1,1)*mat(2,2)-mat(2,1)**2
	!		stop
	!	endif
		!CHECK IF INVERSE IS CORRECT
	!	LO=matmul(mat,matInv)
	!	forall (i=1:Ndim) LO(i,i)=LO(i,i)-1._8
	!	if(sum(LO**2)>eps) stop "Error on matrix inverse!"		
	end function sym_mat_inverse

	subroutine fill_permutations(k4,i1,i2,i3,i4)
		implicit none
		real(8), intent(inout) :: k4(:,:,:,:)
		integer, intent(in) :: i1,i2,i3,i4
		integer :: perm(0:4),j,k,l
		
		perm(:)=(/ 0,i4,i3,i2,i1 /)	
		do
			j=3
			do
				if(perm(j)<perm(j+1)) exit
				j=j-1
			enddo
			if(j==0) exit
			l=4
			do
				if(perm(j)<perm(l)) exit
				l=l-1
			enddo
			call swap_int(perm(j),perm(l))
			k=j+1 ; l=4
			do
				if(k>=l) exit
				call swap_int(perm(k),perm(l))
				k=k+1
				l=l-1
			enddo	
			k4(perm(4),perm(3),perm(2),perm(1))=k4(i1,i2,i3,i4)	
		enddo

	end subroutine fill_permutations

	!GENERATE GAUSSIAN RANDOM NUMBERS FROM UNIFORM DISTRIBUTION (BOX-MULLER)
	subroutine RandGauss(R)
		implicit none
		real(8), intent(out) :: R
		real(8) :: u1,u2,w,rnd(2)	

		do
			call random_number(rnd)
			u1=2*rnd(1)-1
			u2=2*rnd(2)-1
			w=u1*u1+u2*u2
			if (w<1) exit
		enddo
		w=sqrt(-2*log(w)/w)
		R=u1*w
		
	end subroutine RandGauss

	subroutine RandGaussN(R)
		implicit none
		real(8), intent(inout) :: R(:)
		real(8) :: u1,u2,w,rnd(2)
		integer :: p,rest,i,n
		
		n=size(R)
		p=n/2
		rest=mod(n,2)
		do i=1,2*p-1,2
			do
				call random_number(rnd)
				u1=2*rnd(1)-1
				u2=2*rnd(2)-1
				w=u1*u1+u2*u2
				if (w<1) exit
			enddo
			w=sqrt(-2*log(w)/w)
			R(i)=u1*w
			R(i+1)=u2*w
		enddo
		if(rest/=0) call RandGauss(R(n))
		
	end subroutine RandGaussN

	! set random seed
	subroutine set_random_seed()
	implicit none
	integer :: ns, k
	integer, dimension(:), allocatable :: s ! seed setup array
		call random_seed(size=ns) ; allocate(s(ns))    ! set seed for random numbers
		do k = 1, ns ; call system_clock(s(k)) ; enddo
		call random_seed(put=s) ; deallocate(s)
	end subroutine set_random_seed

	subroutine swap_int(a,b)
		implicit none
		integer, intent(inout) :: a,b
		integer :: tmp

		tmp=a
		a=b
		b=tmp
	end subroutine swap_int

end module misc_subroutines
