!> @brief	LaPIM code for sampling the Wigner density using a Langevin dynamics.
!>			The Langevin dynamics is obtained from the PIM method and is performed
!>			in module 'la_pim'.
!>			"Quantum forces" must be used for the langevin dynamics and are computed
!>			in an auxiliary Monte Carlo in module 'm_forces'.
!>			Compatible model potentials are found in module 'potentials'

!-------- BEGINING OF MAIN PROGRAM --------
program RPMD
	use glob_types
	use la_pim
	use misc_subroutines
	use m_io
	use potentials
	IMPLICIT NONE

	TYPE(IO_type) :: IO
	TYPE(sampling_parameters_type) :: sampling_param, therm_param
	TYPE(polymer_type) :: polymer

	REAL(8) :: R
	CHARACTER(2) :: continue_arg, numProc="01"
	INTEGER :: i

!-------- RANDOM SEED --------
	call set_random_seed() !(module 'misc_subroutines')
	write(*,*)
	write(*,*) "****** TEST RANDOM SEED ******"
	do i=1,10
		call random_number(R)
		write(*,*) R
	enddo
	write(*,*) "******************************"
	write(*,*)
	
!-------- INITIALIZATION --------


	! INITIALIZE TO DEFAULT VALUES
	call default_values(IO,sampling_param,therm_param,polymer,pot_param)
	! GET INPUT FILE FROM COMMAND ARGUMENT 
	call get_command_argument(1,IO%nmlfile)
	! GET PARAMETERS FROM INPUT FILE (module 'm_io')
	call read_input(IO,sampling_param,therm_param,polymer,pot_param)
	! ASSIGN POTENTIAL TO CORRECT FUNCTIONS (module 'potentials')
	call assign_potential(sampling_param%Ndim,sampling_param%pot_name)


	! GET NUMBER OF PROCESS FROM COMMAND ARGUMENT
	call get_command_argument(2,numProc)
	! ... AND ADAPT WORKING DIRECTORY CONSEQUENTLY
	IO%filedir=trim(IO%filedir)//"_"//numProc//"/"


	! SET IF TRAJECTORY MUST BE RESTORED AND CONTINUED
	call get_command_argument(3,continue_arg)
	if(continue_arg=='-c' .or. continue_arg=='-C') IO%continue_traj=.TRUE.
	!GET STORED LAST CONFIGURATION IF CONTINUE TRAJECTORY (module 'm_io')
	if(IO%continue_traj) call load_last_config(IO,polymer,sampling_param)


	
	! OPEN OUTPUT FILES (module 'm_io')
	call open_output_files(IO,polymer%nu)
	! WRITE INFO FILE CONTAINING ALL PARAMETERS (module 'm_io')
	call write_info_file(IO,sampling_param,therm_param,polymer,pot_param)
	
	write(*,*) "current directory : "//trim(IO%outdir)//"/"//trim(IO%filedir)


	!COMPUTE TRANSFER MATRIX
	call compute_transfer_matrix(polymer,sampling_param)
	call write_polymer_frequencies(IO,polymer)
		

!-------- THERMALIZATION --------

	if(.not. IO%continue_traj) then
		write(*,*)
		write(*,*) "STARTING",therm_param%Langevin_Nsteps,"STEPS OF THERMALIZATION..."
		
		!ASSIGN frict DEPENDING ON TRPMD
		call compute_frict(polymer,therm_param)
		! LANGEVIN DYNAMICS FOR THERMALIZATION (module 'la_pim')
		call langevin_dynamics(IO,therm_param,polymer)

		write(*,*) "Thermalization done in ",therm_param%timeFinish-therm_param%timeStart,"s."

		call save_last_config(IO,polymer,sampling_param,0)
	endif

!-------- LANGEVIN DYNAMICS --------

	write(*,*)	
	write(*,*) "STARTING",sampling_param%Langevin_Nsteps,"STEPS OF LANGEVIN..."

	!ASSIGN frict DEPENDING ON TRPMD
	call compute_frict(polymer,sampling_param)

	call langevin_dynamics(IO,sampling_param,polymer) ! (module 'la_pim')




!-------- FINALIZATION --------

	!SAVE LAST CONFIGURATION  (module 'm_io')
	call save_last_config(IO,polymer,sampling_param &
			,sampling_param%Langevin_Nsteps+sampling_param%Langevin_Nsteps_prev)

	! PRINT EXECUTION TIME
	if(IO%continue_traj) then
		write(*,*) "Job done in ",sampling_param%timeFinish-sampling_param%timeStart,"seconds !"
		write(IO%info_file_unit,*) "Run time: ",sampling_param%timeFinish-sampling_param%timeStart,"s."	
	else
		write(*,*) "Job done in ",sampling_param%timeFinish-therm_param%timeStart,"seconds!"
		write(IO%info_file_unit,*) "Run time: ",sampling_param%timeFinish-therm_param%timeStart,"s."
	endif	

	call save_and_reset_tmp_files(IO,polymer%nu)
	call close_files(IO,polymer%nu) ! (module 'm_io')

!-------- END OF PROGRAM --------

!-----------------------------------------------------------------------
CONTAINS

!-------- SET DEFAULT VALUES --------
	subroutine default_values(IO,sampling_param,therm_param,polymer,pot_param)
		IMPLICIT NONE
		TYPE(IO_type), INTENT(inout) :: IO
		TYPE(sampling_parameters_type), INTENT(inout) :: sampling_param,therm_param
		TYPE(polymer_type), INTENT(inout) :: polymer
		TYPE(potential_parameters_type), INTENT(inout) :: pot_param

		!IO PARAMETERS
			IO%nmlfile="UNDEFINED"
			IO%outdir="output_default"
			IO%filedir="proc"
			IO%bead_traj_dir="bead_trajectories"
			IO%formatted_output=.FALSE.
			IO%save_all_traj=.TRUE.

			IO%max_count_restore=0
			IO%count_restore=0

			IO%X_file_unit=10
			IO%P_file_unit=11
			IO%Xbeads_file_unit=100

			IO%info_file_unit=1
			IO%last_config_unit=2
			IO%continue_traj=.FALSE.

			IO%save_frequency=0
			IO%notify_frequency=0

		!SAMPLING PARAMETERS
			sampling_param%save_traj=.TRUE.
			sampling_param%pot_name="UNDEFINED"

			sampling_param%Langevin_Nsteps=100
			sampling_param%Langevin_Nsteps_prev=0
			sampling_param%Langevin_nMTS=1

			sampling_param%Ndim=0
			sampling_param%nu=8
			sampling_param%temperature=0
			sampling_param%dt=0

			sampling_param%TRPMD=.FALSE.

			sampling_param%lambda_omk = 0.5

		!THERMALIZATION PARAMETERS
			therm_param=sampling_param
			therm_param%save_traj=.FALSE.
			therm_param%Langevin_Nsteps=0

		!POLYMER PARAMETERS
			polymer%Ndim=sampling_param%Ndim
			polymer%nu=sampling_param%nu
			polymer%time=0

		!POTENTIAL PARAMETERS
			!MORSE AND DOUBLE MORSE
				pot_param%D=20. 		!kcalpermol
				pot_param%alpha=2.5		!1/Angstrom
				!MORSE	
				pot_param%xmax=2.5		!Angstrom
				!DOUBLE MORSE
				pot_param%dAB=2.6		!Angstrom
				pot_param%d0=0.95		!Angstrom
				pot_param%ADM=2.32e5	!kcalpermol
				pot_param%BDM=3.15		!1/Angstrom
				pot_param%CDM=2.31e4	!kcalpermol*Angstrom^6
				pot_param%asym=1.		!Angstrom
			!QUARTIC OSCILLATOR
				pot_param%QO=0.1
			!COUPLED HARMONIC OSCILLATORS
				pot_param%c3=0.1
				pot_param%c4=0

	end subroutine default_values

	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!	
	!! DETERMINATION OF TRANSFER MATRIX !!
	!!       TO NORMAL COORDINATES      !!
	!!         OF BEADS COUPLING        !!
	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	subroutine compute_transfer_matrix(polymer,sampling_param)
		implicit none
		TYPE(sampling_parameters_type), INTENT(in) :: sampling_param
		TYPE(polymer_type), INTENT(inout) :: polymer
		integer :: i,j,INFO,nu
		real(8),allocatable :: WORK(:)
		
		nu=polymer%nu

		allocate(WORK(3*nu-1))
		
		polymer%EigMat=0
		do j=1,nu
			do i=1,nu
				if(i==j) polymer%EigMat(i,j)=2
				if(j==i+1) polymer%EigMat(i,j)=-1
			enddo
		enddo
		polymer%EigMat(1,nu)=-1
		
		call DSYEV('V','U',nu,polymer%EigMat,nu,polymer%OmK,WORK,3*nu-1,INFO)		
		if(INFO/=0) then
			print *,"PB WITH EIGMAT !"
		endif
		
		polymer%OmK(1)=0
		polymer%OmK(:)=sqrt(polymer%OmK(:))*nu*kb*sampling_param%temperature/hbar
		
		!COMPUTE INVERSE TRANSFER MATRIX
		polymer%EigMatTr=transpose(polymer%EigMat)
		if(nu==1) write(*,*) "EigMat=",polymer%EigMat
		
		deallocate(WORK)
		
	end subroutine compute_transfer_matrix

	subroutine compute_frict(polymer,sampling_param)
		implicit none
		TYPE(sampling_parameters_type), INTENT(in) :: sampling_param
		TYPE(polymer_type), INTENT(inout) :: polymer
		INTEGER :: i,k

		if(sampling_param%TRPMD) then
			do k=1,polymer%Ndim					
				do i=1,polymer%nu
					polymer%frict(i,k)=max(2*sampling_param%lambda_omk*polymer%Omk(i),sampling_param%damping(k))
				enddo		
			enddo
		else
			do i=1,polymer%nu
				polymer%frict(i,:)=sampling_param%damping(:)
			enddo
		endif

	end subroutine compute_frict

end program RPMD