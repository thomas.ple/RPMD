module glob_types

!GLOBAL PARAMETERS
	REAL(8), PARAMETER :: hbar=1._8, kb=1._8, pi=4*atan(1._8)
	INTEGER, PARAMETER :: num_aux_MC=3

!GLOBAL TYPES

	!----------------------------------------------------

	TYPE sampling_parameters_type

		REAL :: timeStart
		REAL :: timeFinish
		LOGICAL :: save_traj

		CHARACTER(10) :: pot_name

		INTEGER :: Langevin_Nsteps
		INTEGER :: Langevin_Nsteps_prev
		INTEGER :: Langevin_nMTS

		INTEGER :: Ndim
		INTEGER :: nu
		REAL(8) :: temperature
		REAL(8) :: dt
		REAL(8) :: lambda_omk
		REAL(8), ALLOCATABLE :: damping(:)
		REAL(8), ALLOCATABLE :: sigma(:)

		REAL(8) :: dBeta
		REAL(8), ALLOCATABLE :: sigp2(:)

		LOGICAL :: TRPMD

	END TYPE sampling_parameters_type

	!----------------------------------------------------

	TYPE polymer_type

		INTEGER :: Ndim
		INTEGER :: nu
		REAL(8) :: time
		REAL(8), ALLOCATABLE :: mass(:)

		REAL(8), ALLOCATABLE :: X(:,:)
		REAL(8), ALLOCATABLE :: P(:,:)

		REAL(8), ALLOCATABLE :: EigX(:,:)
		REAL(8), ALLOCATABLE :: EigP(:,:)

		REAL(8), ALLOCATABLE :: OmK(:)
		REAL(8), ALLOCATABLE :: EigMatTr(:,:)
		REAL(8), ALLOCATABLE :: EigMat(:,:)

		REAL(8), ALLOCATABLE :: frict(:,:)

	END TYPE polymer_type

	!----------------------------------------------------

	TYPE IO_type
		character(len=200) :: nmlfile

		CHARACTER (len=200) :: outdir
		CHARACTER (len=200) :: filedir
		CHARACTER (len=200) :: bead_traj_dir
		LOGICAL :: formatted_output
		LOGICAL :: save_all_traj

		INTEGER :: max_count_restore
		INTEGER :: count_restore

		INTEGER :: X_file_unit
		INTEGER :: P_file_unit
		INTEGER :: Xbeads_file_unit

		INTEGER :: info_file_unit
		CHARACTER (len=15) :: info_file="parameters.info"
		INTEGER :: last_config_unit
		CHARACTER (len=16) :: last_config_file="last_config.info"
		LOGICAL :: continue_traj

		INTEGER :: save_frequency
		INTEGER :: notify_frequency
	END TYPE IO_type

	!----------------------------------------------------

	TYPE potential_parameters_type
		!MORSE AND DOUBLE MORSE POTENTIAL
		REAL(8) :: D
		REAL(8) :: alpha
			!MORSE
			REAL(8) ::	xmax
			!DOUBLE MORSE
			REAL(8) :: dAB
			REAL(8) :: d0
			REAL(8) :: ADM
			REAL(8) :: BDM
			REAL(8) :: CDM
			REAL(8) :: asym
		!HARMONIC OSCILLATOR
		REAL(8), ALLOCATABLE :: mass_HO(:)
		REAL(8), ALLOCATABLE ::  Omega0(:)
		!QUARTIC OSCILLATOR
		REAL(8) :: QO=0.1
		!COUPLED OSCILLATORS
		REAL(8) :: c3=0
		REAL(8) :: c4=0

	END TYPE potential_parameters_type

!---------------------------------------------------------------------------

!! Taken from PaPIM

	!> @param Convert a integer into character format
	INTERFACE num_to_char
    	MODULE PROCEDURE integer_to_char
    	MODULE PROCEDURE real_to_char
	END INTERFACE num_to_char
	PUBLIC :: num_to_char

	!> @param Character case conversion
	PUBLIC :: to_upper_case
	PUBLIC :: to_lower_case

	!> @param Get current date and time (in dd-mm-yyyy hh:mm:ss.sss format)
  	PUBLIC :: get_dateandtime

CONTAINS

	PURE FUNCTION real_to_char( in_number ) RESULT( num_as_char )
		!> @brief Make a character of a real number. The limit is forty digits, 
		!> including the number sign.
		!>
		!> Input variable:
		!>    in_number - Real number.
		!> Output variable
		!>    num_as_char - Integer represented as character.
		!>
		REAL(8), INTENT( IN ) :: in_number
		CHARACTER( LEN=50 ) :: num_as_char

		!--------------------------------------------------------------------------

		WRITE( num_as_char, '(f50.1)' ) in_number
		num_as_char = TRIM( ADJUSTL( num_as_char(1:INDEX( num_as_char, '.' )-1) ) )


	END FUNCTION real_to_char

	!****************************************************************************

	PURE FUNCTION integer_to_char( in_number ) RESULT( num_as_char )
		!> @brief Make a character of an integer type 4 number. The limit is ten digits, 
		!> including the number sign.
		!>
		!> Input variable:
		!>    in_number - Integer number in range [-999999999,2147483647].
		!> Output variable
		!>    num_as_char - Integer represented as character.
		!>
		INTEGER, INTENT( IN ) :: in_number
		CHARACTER( LEN=10 )   :: num_as_char

		!--------------------------------------------------------------------------

		WRITE( num_as_char, '(i10)' ) in_number
		num_as_char = TRIM( ADJUSTL( num_as_char ) )


	END FUNCTION integer_to_char

	!****************************************************************************

	PURE FUNCTION to_upper_case( str ) RESULT( string )
		!> @brief Changes a string to upper case

		CHARACTER( LEN=* ), INTENT(IN) :: str
		CHARACTER( LEN=LEN(str) )      :: string

		!! Localvariables
		INTEGER :: ic
		INTEGER :: i
		CHARACTER( LEN=26 ), PARAMETER :: cap = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
		CHARACTER( LEN=26 ), PARAMETER :: low = 'abcdefghijklmnopqrstuvwxyz'

		!--------------------------------------------------------------------------

		!! Capitalize each letter if it is lowecase
		string = str
		DO i = 1, LEN_TRIM(str)
		ic = INDEX(low, str(i:i))
		IF (ic > 0) string(i:i) = cap(ic:ic)
		END DO


	END FUNCTION to_upper_case

	!****************************************************************************

	PURE FUNCTION to_lower_case( str ) RESULT( string )
		!> @brief Changes a string to lower case

		CHARACTER( LEN=* ), INTENT(IN) :: str
		CHARACTER( LEN=LEN(str) )      :: string

		!! Local variables
		INTEGER :: ic
		INTEGER :: i
		CHARACTER( LEN=26 ), PARAMETER :: cap = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
		CHARACTER( LEN=26 ), PARAMETER :: low = 'abcdefghijklmnopqrstuvwxyz'

		!--------------------------------------------------------------------------

		!! Capitalize each letter if it is lowecase
		string = str
		DO i = 1, LEN_TRIM(str)
		ic = INDEX(cap, str(i:i))
		IF (ic > 0) string(i:i) = low(ic:ic)
		END DO


	END FUNCTION to_lower_case

	!****************************************************************************

	FUNCTION get_dateandtime() RESULT( dateandtime )

		!> @brief Get current date and time in "dd-mm-yyyy hh:mm:ss.sss" format

		CHARACTER( LEN=23 ) :: dateandtime

		!! Local variable
		INTEGER, DIMENSION(8) :: time_val

		!--------------------------------------------------------------------------

		CALL DATE_AND_TIME( values=time_val )

		WRITE( dateandtime, '(2(i2.2,a),i4,a,3(i2.2,a),i3.3)' ) time_val(3), '-', time_val(2), &
		&'-', time_val(1), ' ', time_val(5), ':',  time_val(6), ':', time_val(7), '.', time_val(8)
		dateandtime = TRIM( dateandtime )


	END FUNCTION get_dateandtime

	!****************************************************************************


end module glob_types