MODULE la_pim
	use glob_types
	use m_io
	use potentials
	use misc_subroutines

	IMPLICIT NONE

CONTAINS

	subroutine langevin_dynamics(IO,sampling_param,polymer)
		IMPLICIT NONE

		TYPE(IO_type), INTENT(inout) :: IO
		TYPE(sampling_parameters_type), INTENT(INOUT) :: sampling_param
		TYPE(polymer_type), INTENT(INOUT) :: polymer

		INTEGER :: Ndim
		INTEGER :: i,k,load
		REAL(8) :: dt
		REAL :: timeInt

		load=1

		dt=sampling_param%dt
		Ndim=sampling_param%Ndim

		call cpu_time(sampling_param%timeStart)

		do i=1,sampling_param%Langevin_Nsteps

			!PROPAGATE X'=P/M
			call apply_A(polymer,dt)

			! PROPAGATE PIM FORCES (B) AND LANGEVIN FORCES (O)
			call apply_B(polymer,dt/2)
			call apply_O(polymer,sampling_param,dt)
			call apply_B(polymer,dt/2)

			if(sampling_param%save_traj) then ! NOT THERMALIZATION: SAVE TRAJECTORY IF NEEDED
				!UPDATE TIME
				polymer%time=polymer%time+dt
				
				!UPDATE NORMAL MODES
				do k=1,Ndim
					polymer%EigX(:,k)=matmul(polymer%EigMatTr,polymer%X(:,k))
					polymer%EigP(:,k)=matmul(polymer%EigMatTr,polymer%P(:,k))
				enddo

				!WRITE CONFIGURATION
				call write_config(IO,polymer)
				if(MOD(i,IO%save_frequency)==0) then
					call save_and_reset_tmp_files(IO,polymer%nu)
					call save_last_config(IO,polymer,sampling_param,i+sampling_param%Langevin_Nsteps_prev)
					write(*,*) "config saved."
				endif
				if(MOD(i,IO%notify_frequency)==0) then
					call cpu_time(timeInt)
					write(*,*) "notification:",i,"steps in",INT(timeInt-sampling_param%timeStart),"s."
				endif
			endif

			! UPDATE PROGRESSION BAR
			if(i>=load*5*sampling_param%Langevin_Nsteps/100) then
				do while(i>=load*5*sampling_param%Langevin_Nsteps/100)
					load=load+1	
				enddo
					call cpu_time(timeInt)
					do k=1,30
						write(*,'(a1)',advance='no') char(8)
					enddo
					write(*,'(i3,a16,i6,a1)',advance='no') (load-1)*5,"% completed in :",nint(timeInt-sampling_param%timeStart),"s"									
			endif
			
		enddo
		write(*,*)
		call cpu_time(sampling_param%timeFinish)

	end subroutine langevin_dynamics

!-------------------------------------------------
!	DEFINE EVOLUTION SUBROUTINES

	subroutine apply_B(polymer,tau)
		IMPLICIT NONE		
		TYPE(polymer_type), INTENT(inout) :: polymer
		REAL(8), INTENT(in) :: tau
		INTEGER :: i
		
		do i=1,polymer%nu
			polymer%P(i,:)=polymer%P(i,:)-tau*dPot(polymer%X(i,:))
		enddo

		
	end subroutine apply_B

	subroutine apply_A(polymer,tau)
		IMPLICIT NONE		
		TYPE(polymer_type), INTENT(inout) :: polymer
		REAL(8), INTENT(in) :: tau
		REAL(8), ALLOCATABLE, SAVE :: EigX0(:), EigP0(:)
		INTEGER :: k,i

		if(.not. allocated(EigX0)) allocate(EigX0(polymer%nu), EigP0(polymer%nu))
		
		do k=1,polymer%Ndim
			EigP0=matmul(polymer%EigMatTr,polymer%P(:,k))
			EigX0=matmul(polymer%EigMatTr,polymer%X(:,k))

			polymer%EigX(1,k)=EigX0(1)+tau*EigP0(1)/polymer%mass(k)
			polymer%EigP(1,k)=EigP0(1)	
			do i=2,polymer%nu
				polymer%EigX(i,k)=EigX0(i)*cos(polymer%OmK(i)*tau) &
								+EigP0(i)*sin(polymer%OmK(i)*tau)/(polymer%mass(k)*polymer%OmK(i))
				polymer%EigP(i,k)=EigP0(i)*cos(polymer%OmK(i)*tau) &
								-polymer%mass(k)*polymer%OmK(i)*EigX0(i)*sin(polymer%OmK(i)*tau)
			enddo
			
			polymer%X(:,k)=matmul(polymer%EigMat,polymer%EigX(:,k))
			polymer%P(:,k)=matmul(polymer%EigMat,polymer%EigP(:,k))
		enddo
		
	end subroutine apply_A

	subroutine apply_O(polymer,SP,tau)
		implicit none
		TYPE(sampling_parameters_type), INTENT(IN) :: SP
		TYPE(polymer_type), INTENT(inout) :: polymer
		REAL(8), INTENT(in) :: tau
		REAL(8) :: R(polymer%nu)
		INTEGER :: i,k

		do k=1,polymer%Ndim
			polymer%EigP(:,k)=matmul(polymer%EigMatTr,polymer%P(:,k))
			call RandGaussN(R)
			do i=1,polymer%nu
				polymer%EigP(i,k)=polymer%EigP(i,k)*exp(-polymer%frict(i,k)*tau) &
						+hbar*sqrt((2/SP%sigp2(k))*polymer%frict(i,k)) &
							*sqrt((1-exp(-2*polymer%frict(i,k)*tau))/(2*polymer%frict(i,k)))*R(i)
			enddo
			polymer%P(:,k)=matmul(polymer%EigMat,polymer%EigP(:,k))
		enddo

	end subroutine apply_O

END MODULE la_pim