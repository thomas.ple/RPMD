MODULE m_io
	use glob_types
	use atomic_units
	use parser_module, ONLY : split_line, load_values, PRL_abort

	IMPLICIT NONE

CONTAINS

	subroutine write_config(IO,polymer)
		IMPLICIT NONE
		TYPE(IO_type), INTENT(in) :: IO
		TYPE(polymer_type), INTENT(IN) :: polymer
		INTEGER :: i

		if(IO%formatted_output) then

			write(IO%X_file_unit,*) polymer%time*fs, (sum(polymer%X,DIM=1)/polymer%nu)*bohr
			write(IO%P_file_unit,*) polymer%time*fs, (sum(polymer%P,DIM=1)/polymer%nu)/polymer%mass(:)*(bohr/fs)
			if(IO%save_all_traj) then
				do i=1,polymer%nu
					write(IO%Xbeads_file_unit+i-1,*) polymer%time*fs, polymer%X(i,:)*bohr
				enddo
			endif

		else

			write(IO%X_file_unit) polymer%time*fs, (sum(polymer%X,DIM=1)/polymer%nu)*bohr
			write(IO%P_file_unit) polymer%time*fs, (sum(polymer%P,DIM=1)/polymer%nu)/polymer%mass(:)*(bohr/fs)
			if(IO%save_all_traj) then
				do i=1,polymer%nu
					write(IO%Xbeads_file_unit+i-1) polymer%time*fs, polymer%X(i,:)*bohr
				enddo
			endif

		endif

		FLUSH (IO%X_file_unit)
		FLUSH (IO%P_file_unit)
	!	if(IO%save_all_traj) then
	!		do i=1,polymer%nu
	!			FLUSH (IO%Xbeads_file_unit+i-1)
	!		enddo
	!	endif
	end subroutine write_config

!---------------------------------------------------------------

	subroutine save_last_config(IO,polymer,sampling_param,nsteps)
		IMPLICIT NONE
		TYPE(IO_type), INTENT(in) :: IO
		TYPE(sampling_parameters_type), INTENT(in) :: sampling_param
		TYPE(polymer_type), INTENT(in) :: polymer
		INTEGER, INTENT(IN) :: nsteps

		open(IO%last_config_unit,file=trim(IO%outdir)//"/"//trim(IO%filedir)//IO%last_config_file,form="unformatted")
			write(IO%last_config_unit) polymer%time
			write(IO%last_config_unit) nsteps
			write(IO%last_config_unit) sampling_param%sigma
			write(IO%last_config_unit) polymer%X(:,:)
			write(IO%last_config_unit) polymer%P(:,:)
		close(IO%last_config_unit)

	end subroutine save_last_config

	subroutine load_last_config(IO,polymer,sampling_param)
		IMPLICIT NONE
		TYPE(IO_type), INTENT(inout) :: IO
		TYPE(sampling_parameters_type), INTENT(inout) :: sampling_param
		TYPE(polymer_type), INTENT(inout) :: polymer
		LOGICAL :: file_exists

		inquire(file=trim(IO%outdir)//"/"//trim(IO%filedir)//IO%last_config_file, exist=file_exists)
		if(.not. file_exists) then
			write(0,*) "Error: Last configuration file not present. ("//trim(IO%outdir)//"/"//trim(IO%filedir)//IO%last_config_file//")"
			write(0,*) "Execution stopped!"
			stop
		endif

		open(IO%last_config_unit,file=trim(IO%outdir)//"/"//trim(IO%filedir)//IO%last_config_file,form="unformatted")
			read(IO%last_config_unit) polymer%time
			read(IO%last_config_unit) sampling_param%Langevin_Nsteps_prev
			read(IO%last_config_unit) sampling_param%sigma
			read(IO%last_config_unit) polymer%X(:,:)
			read(IO%last_config_unit) polymer%P(:,:)
		close(IO%last_config_unit)

		write(*,*) "Configuration retrieved successfully."

	end subroutine load_last_config

!---------------------------------------------------------------

	subroutine open_output_files(IO,nu)
		IMPLICIT NONE
		TYPE(IO_type), INTENT(in) :: IO
		INTEGER, INTENT(in) :: nu
		CHARACTER(11) :: FileFormat="unformatted"
		CHARACTER(3) :: num_bead="XXX"
		LOGICAL :: file_exists
		INTEGER :: i
		
		if(.not. IO%continue_traj) then
			call system("mkdir "//trim(IO%outdir))
			call system("mkdir "//trim(IO%outdir)//"/"//trim(IO%filedir))
			if(IO%save_all_traj) call system("mkdir "//trim(IO%outdir)//"/"&
										&//trim(IO%filedir)//"/"//trim(IO%bead_traj_dir))

			inquire(file=trim(IO%outdir)//"/"//trim(IO%filedir)//"X.traj", exist=file_exists)
			if(file_exists) then
				call system("rm "//trim(IO%outdir)//"/"//trim(IO%filedir)//"X.traj")
				call system("rm "//trim(IO%outdir)//"/"//trim(IO%filedir)//"V.traj")
				if(IO%save_all_traj) then
					call system("rm "//trim(IO%outdir)//"/"//trim(IO%filedir)//"/"&
							&//trim(IO%bead_traj_dir)//"/X_bead*.traj")
				endif
			endif
		endif
		
		if(IO%formatted_output) FileFormat="formatted"
		write(*,*) "I/O : format="//FileFormat

		open(IO%X_file_unit,form=trim(FileFormat) ,ACCESS="STREAM" &!, STATUS="SCRATCH" &
			,file=trim(IO%outdir)//"/"//trim(IO%filedir)//"X.traj.tmp")

		open(IO%P_file_unit,form=trim(FileFormat) ,ACCESS="STREAM" &!, STATUS="SCRATCH" &
			,file=trim(IO%outdir)//"/"//trim(IO%filedir)//"P.traj.tmp")
		
		if(IO%save_all_traj) then
			do i=1,nu
				write(num_bead,'(i3.3)') i
				open(IO%Xbeads_file_unit+i-1,form=trim(FileFormat) ,ACCESS="STREAM" &!, STATUS="SCRATCH" &
					,file=trim(IO%outdir)//"/"//trim(IO%filedir)//"/"&
						&//trim(IO%bead_traj_dir)//"/X_bead"//num_bead//".traj.tmp")
			enddo
		endif

		write(*,*) "I/O : open output files OK."

	end subroutine open_output_files

	subroutine save_and_reset_tmp_files(IO,nu)
		IMPLICIT NONE
		TYPE(IO_type), INTENT(in) :: IO
		INTEGER, INTENT(in) :: nu
		CHARACTER(3) :: num_bead="XXX"
		integer :: i

		call system("cat "//trim(IO%outdir)//"/"//trim(IO%filedir)//"X.traj.tmp >> &
			&"//trim(IO%outdir)//"/"//trim(IO%filedir)//"X.traj")
		REWIND( UNIT=IO%X_file_unit ) 

		call system("cat "//trim(IO%outdir)//"/"//trim(IO%filedir)//"P.traj.tmp >> &
			&"//trim(IO%outdir)//"/"//trim(IO%filedir)//"P.traj")
		REWIND( UNIT=IO%P_file_unit )

		if(IO%save_all_traj) then
			do i=1,nu
				write(num_bead,'(i3.3)') i
				call system("cat "//trim(IO%outdir)//"/"//trim(IO%filedir)//"/"&
						&//trim(IO%bead_traj_dir)//"/X_bead"//num_bead//".traj.tmp >> &
						&"//trim(IO%outdir)//"/"//trim(IO%filedir)//"/"&
						&//trim(IO%bead_traj_dir)//"/X_bead"//num_bead//".traj")
				REWIND( UNIT=IO%Xbeads_file_unit+i-1 )
			enddo
		endif

	end subroutine save_and_reset_tmp_files

	subroutine close_files(IO,nu)
		IMPLICIT NONE
		TYPE(IO_type), INTENT(in) :: IO
		INTEGER, INTENT(in) :: nu
		INTEGER :: i

		close(IO%X_file_unit,STATUS="DELETE")
		close(IO%P_file_unit,STATUS="DELETE")
		close(IO%info_file_unit,STATUS="DELETE")
		do i=1,nu
			close(IO%Xbeads_file_unit+i-1,STATUS="DELETE")
		enddo

		close(IO%info_file_unit)
		write(*,*) "I/O : close output files OK."

	end subroutine close_files

!---------------------------------------------------------------

	subroutine write_info_file(IO,sampling_param,therm_param,polymer,pot_param)
		IMPLICIT NONE
		TYPE(IO_type), INTENT(in) :: IO
		TYPE(sampling_parameters_type), INTENT(in) :: sampling_param,therm_param
		TYPE(polymer_type), INTENT(in) :: polymer
		TYPE(potential_parameters_type), INTENT(in) :: pot_param
		integer :: u,i

		u=IO%info_file_unit

		open(u,file=trim(IO%outdir)//"/"//trim(IO%filedir)//IO%info_file &
				,ACCESS="STREAM",FORM="FORMATTED")

			write(u,*) "namelist file :", trim(IO%nmlfile)

			write(u,*)

			write(u,*) "SYSTEM INFO :"
			write(u,*) "Ndim=",polymer%Ndim
			write(u,*) "nu=",polymer%nu
			write(u,*) "Nsteps=",sampling_param%Langevin_Nsteps+sampling_param%Langevin_Nsteps_prev
			write(u,*) "thermSteps=",therm_param%Langevin_Nsteps
			write(u,*) "dt=",sampling_param%dt*fs,"fs"
			write(u,*) "T=",sampling_param%temperature*kelvin,"K"
			write(u,*) "lambda_omk=",sampling_param%lambda_omk

			do i=1,polymer%Ndim
				write(u,*)
				write(u,'(A9,I2)') "DIMENSION", i
				write(u,*) "	mass=", polymer%mass(i)/Mprot,"units of proton mass"
				write(u,*) "	damping P=",sampling_param%damping(i)*THz/(2*pi),"THz"
				write(u,*) "	(RndRate P **2=",sampling_param%sigma(i)**2,")"
				write(u,*) "	damping Them=",therm_param%damping(i)*THz/(2*pi),"THz"
			enddo
			
			write(u,*)

			!WRITE POTENTIAL PARAMETERS
			SELECT CASE( trim(sampling_param%pot_name))
			CASE("DM")
				write(u,*) "POTENTIAL : DOUBLE MORSE"
				write(u,*) "D=",pot_param%D*kcalperMol,"kcal/mol"
				write(u,*) "alpha=",pot_param%alpha/bohr,"A^-1"
				write(u,*) "asym=",pot_param%asym
			CASE("HO")
				write(u,*) "POTENTIAL : HARMONIC OSCILLATORS"
				do i=1,polymer%Ndim
					write(u,*) "Omega0(",i,")=",pot_param%Omega0(i)*THZ/(2*pi),"THz"		
				enddo
			CASE("QO")
				write(u,*) "POTENTIAL : QUARTIC OSCILLATOR"
				write(u,*) "QO=",pot_param%QO,"a.u."
			CASE("MO")
				write(u,*) "POTENTIAL : MORSE"
				write(u,*) "D=",pot_param%D*kcalperMol,"kcal/mol"
				write(u,*) "alpha=",pot_param%alpha/bohr,"A^-1"
				write(u,*) "xmax=",pot_param%xmax*bohr,"A"	
			CASE("CO")
				write(u,*) "POTENTIAL : COUPLED HARMONIC OSCILLATORS"
				do i=1,polymer%Ndim
					write(12,*) "Omega0(",i,")=",pot_param%Omega0(i)*THZ/(2*pi),"THz"		
				enddo
				write(u,*) "c3=",pot_param%c3
				write(u,*) "c4=",pot_param%c4
			CASE("DM1D")
				write(u,*) "POTENTIAL : DOUBLE MORSE 1D"
				write(u,*) "dAB=", pot_param%dAB*bohr,"A"
				write(u,*) "D=",pot_param%D*kcalperMol,"kcal/mol"
				write(u,*) "alpha=",pot_param%alpha/bohr,"A^-1"
				write(u,*) "asym=",pot_param%asym
			CASE("CO2")
				write(u,*) "POTENTIAL : CO2 (3D)"
				do i=1,polymer%Ndim
					write(12,*) "Omega0(",i,")=",pot_param%Omega0(i)*THZ/(2*pi),"THz"		
				enddo
				write(u,*) "c3=",pot_param%c3
			CASE DEFAULT
				call prl_abort('Error: "'//trim(sampling_param%pot_name)//'" is not a correct pot_name')
			END SELECT

	end subroutine write_info_file	

	subroutine write_polymer_frequencies(IO,polymer)
		IMPLICIT NONE
		TYPE(IO_type), INTENT(in) :: IO
		TYPE(polymer_type), INTENT(in) :: polymer
		integer :: u,i

		u=IO%info_file_unit

		write(u,*) "POLYMER FREQUENCIES:"
		write(*,*) "POLYMER FREQUENCIES:"
		do i=1,polymer%nu
			write(u,*) i,polymer%OmK(i)*THz/(2*pi),"THz (",2*pi*fs/polymer%OmK(i),"fs)"
			write(*,*) i,polymer%OmK(i)*THz/(2*pi),"THz (",2*pi*fs/polymer%OmK(i),"fs)"
		enddo

	end subroutine write_polymer_frequencies

!--------------------------------------------------------------	

	subroutine convert_in_atomic_units(sampling_param,therm_param,polymer,pot_param)
		IMPLICIT NONE
		TYPE(sampling_parameters_type), INTENT(inout) :: sampling_param,therm_param
		TYPE(polymer_type), INTENT(inout) :: polymer
		TYPE(potential_parameters_type), INTENT(inout) :: pot_param

		!SAMPLING PARAMETERS
			sampling_param%temperature=sampling_param%temperature/kelvin
			sampling_param%dt=sampling_param%dt/fs
			sampling_param%damping=sampling_param%damping*2*pi/THz

		!THERMALIZATION PARAMETERS
			therm_param%temperature=therm_param%temperature/kelvin
			therm_param%dt=therm_param%dt/fs
			therm_param%damping=therm_param%damping*2*pi/THz
		
		!POLYMER PARAMETERS
			polymer%mass=polymer%mass*Mprot
			polymer%X=polymer%X/bohr
		!	system%X_prev=system%X_prev/bohr
			polymer%P=polymer%P*Mprot*fs/bohr

		!POTENTIAL PARAMETERS
			pot_param%D=pot_param%D/kcalperMol
			pot_param%alpha=pot_param%alpha*bohr
			pot_param%xmax=pot_param%xmax/bohr
			pot_param%Omega0=pot_param%Omega0*2*pi/THz
			pot_param%d0=pot_param%d0/bohr
			pot_param%ADM=pot_param%ADM/kcalperMol
			pot_param%BDM=pot_param%BDM*bohr
			pot_param%CDM=pot_param%CDM/(kcalperMol*bohr**6)
			pot_param%dAB=pot_param%dAB/bohr
			pot_param%mass_HO=pot_param%mass_HO*Mprot

	end subroutine convert_in_atomic_units

!---------------------------------------------------------------

SUBROUTINE read_input(IO,sampling_param,therm_param,polymer,pot_param)
	IMPLICIT NONE

	TYPE(IO_type), INTENT(inout) :: IO
	TYPE(sampling_parameters_type), INTENT(inout) :: sampling_param,therm_param
	TYPE(potential_parameters_type), INTENT(inout) :: pot_param
	TYPE(polymer_type), INTENT(inout) :: polymer

    ! Local
	CHARACTER(200) :: inputfile
    INTEGER :: Ndim, nu, i
	REAL(8), ALLOCATABLE :: X_init(:)
	REAL(8) :: damping0,damping0_therm

    CHARACTER (LEN = 256) :: line
    CHARACTER (LEN = 256), DIMENSION(256) :: fields
    CHARACTER (LEN = 32), DIMENSION(256)  :: field_types
    CHARACTER (LEN = 256) :: keyword
    LOGICAL :: file_exists

    INTEGER :: iostatus
    INTEGER :: nfields

    !! PARAMETERS section
    LOGICAL :: in_parameters_section
    LOGICAL :: parameters_section_read
	LOGICAL :: ndim_read
	LOGICAL :: nu_read
    LOGICAL :: temperature_read
	LOGICAL :: Langevin_Nsteps_read
	LOGICAL :: mass_read
	
	!! POTENTIAL section
	LOGICAL :: in_potential_section
    LOGICAL :: potential_section_read
	LOGICAL :: pot_name_read

    !--------------------------------------------------------------------------
    
    !! PARAMETERS section found initial status
		in_parameters_section = .FALSE.
		parameters_section_read = .FALSE.
		ndim_read = .FALSE.
		nu_read = .FALSE.
		temperature_read = .FALSE.
		Langevin_Nsteps_read = .FALSE.
		mass_read = .FALSE.
		
	!! POTENTIAL section found initial status
		in_potential_section = .FALSE.
		potential_section_read = .FALSE.
		pot_name_read = .FALSE.


    !! Check input file
	inputfile=IO%nmlfile
    INQUIRE( FILE=trim(inputfile), EXIST=file_exists )
    IF( .NOT.file_exists ) CALL prl_abort( &
        "read_input: Input file <"// &
        TRIM( inputfile )// "> does not exist")

    !! Open input file
    OPEN( FILE=trim(inputfile), UNIT=99 )
    REWIND( UNIT=99 )



    !! Read inputs
   	DO
        READ( UNIT=99, FMT='(A256)', IOSTAT=iostatus ) line
        IF( iostatus /= 0 ) EXIT
            
		CALL split_line( line, nfields, fields, field_types )
	
	!----------------------------------------------
    ! PARAMETERS section
        IF( nfields == 1 .AND. TRIM( to_upper_case( fields(1) ) ) == "&PARAMETERS" ) THEN
            IF( parameters_section_read ) CALL prl_abort( "Error! PARAMETERS section duplicate" )
            parameters_section_read = .TRUE.
            in_parameters_section = .TRUE.
            CYCLE
        ELSE IF( nfields == 2 .AND. ( to_upper_case( fields(1) ) == "&END" &
                &.AND. to_upper_case( fields(2) ) == "PARAMETERS" ) ) THEN
            in_parameters_section = .FALSE.
            CYCLE
        END IF

        IF( in_parameters_section ) THEN
            fields(1) = to_lower_case( fields(1) )
            SELECT CASE( TRIM( fields(1) ) )
            CASE( "" )
                !!Blank line

            CASE( "outdir" )
                CALL load_values(nfields, fields, field_types,keyword,IO%outdir)

			CASE( "filedir" )
                CALL load_values(nfields, fields, field_types,keyword,IO%filedir)

			CASE( "formatted_output" )
                CALL load_values(nfields, fields, field_types,keyword,IO%formatted_output)
			
			CASE( "save_all_traj" )
                CALL load_values(nfields, fields, field_types,keyword,IO%save_all_traj)

			CASE( "max_count_restore" )
                CALL load_values(nfields, fields, field_types,keyword,IO%max_count_restore)
				
			CASE( "ndim" )
                CALL load_values(nfields, fields, field_types,keyword,Ndim)
				if(Ndim<=0) call prl_abort("Error! 'Ndim' is not properly defined.")
				polymer%Ndim=Ndim
				sampling_param%Ndim=Ndim
				therm_param%Ndim=Ndim
				ndim_read=.TRUE.
				allocate(X_init(Ndim),polymer%mass(Ndim))
				allocate(sampling_param%damping(Ndim),sampling_param%sigma(Ndim))
				allocate(therm_param%damping(Ndim),therm_param%sigma(Ndim))
				allocate(pot_param%Omega0(Ndim), pot_param%mass_HO(Ndim))
				sampling_param%damping=10.
				therm_param%damping=-1
				pot_param%Omega0=10.
				polymer%mass=1.
				pot_param%mass_HO=polymer%mass
				X_init=0

			CASE( "nu" )
                CALL load_values(nfields, fields, field_types,keyword,nu)
				if(nu<=0) &
					call prl_abort("Error! 'nu' is not properly defined.")
				sampling_param%nu=nu
				therm_param%nu=nu
				polymer%nu=nu
				nu_read = .TRUE.

			CASE( "langevin_nsteps" )
                CALL load_values(nfields, fields, field_types,keyword,sampling_param%Langevin_Nsteps)
				Langevin_Nsteps_read = .TRUE.
			
			CASE( "therm_nsteps" )
                CALL load_values(nfields, fields, field_types,keyword,therm_param%Langevin_Nsteps)

			CASE( "dt" )
				CALL load_values(nfields, fields, field_types,keyword,sampling_param%dt)
				therm_param%dt=sampling_param%dt

			CASE( "nmts")
				CALL load_values(nfields, fields, field_types,keyword,sampling_param%Langevin_nMTS)
				therm_param%Langevin_nMTS=sampling_param%Langevin_nMTS

			CASE( "temperature" )
				CALL load_values(nfields, fields, field_types,keyword,sampling_param%temperature)
				therm_param%temperature=sampling_param%temperature
				temperature_read = .TRUE.

			CASE( "mass" )
				if(.not. ndim_read) call prl_abort("read_input: &
					 &'Ndim' must be specified before 'mass'!")
				CALL load_values(nfields, fields, field_types,keyword,polymer%mass)
				pot_param%mass_HO=polymer%mass
				mass_read = .TRUE.

			CASE( "damping" )
				if(.not. ndim_read) call prl_abort("read_input: &
					 &'Ndim' must be specified before 'damping'!")
				CALL load_values(nfields, fields, field_types,keyword,damping0)
				sampling_param%damping=damping0
			
			CASE( "damping_therm" )
				if(.not. ndim_read) call prl_abort("read_input: &
					 &'Ndim' must be specified before 'damping_therm'!")
				CALL load_values(nfields, fields, field_types,keyword,damping0_therm)
				therm_param%damping=damping0_therm

			CASE( "xinit" )
				if(.not. ndim_read) call prl_abort("read_input: &
					 &'Ndim' must be specified before 'Xinit'!")
				CALL load_values(nfields, fields, field_types,keyword,X_init)

			CASE( "trpmd" )
                CALL load_values(nfields, fields, field_types,keyword,sampling_param%TRPMD)
				therm_param%TRPMD=sampling_param%TRPMD

			CASE( "lambda_omk" )
                CALL load_values(nfields, fields, field_types,keyword,sampling_param%lambda_omk)
				if(nu<=0) &
					call prl_abort("Error! 'lambda_omk' must be strictly positive.")
				therm_param%lambda_omk=sampling_param%lambda_omk

			CASE ( "save_frequency" )
				CALL load_values(nfields, fields, field_types,keyword,IO%save_frequency)

			CASE ( "notify_frequency" )
				CALL load_values(nfields, fields, field_types,keyword,IO%notify_frequency)

			CASE( "/" )		
				in_parameters_section = .FALSE.

            CASE DEFAULT
                CALL prl_abort( "read_input: Unknown keyword &
                    &in section '&PARAMETERS ... &END PARAMETERS:"//TRIM( fields(1) ) )
            END SELECT
        END IF


	!----------------------------------------------
    ! POTENTIAL section
        IF( nfields == 1 .AND. TRIM( to_upper_case( fields(1) ) ) == "&POTENTIAL" ) THEN
            IF( potential_section_read ) CALL prl_abort( "Error! POTENTIAL section duplicate" )
            potential_section_read = .TRUE.
            in_potential_section = .TRUE.
            CYCLE
        ELSE IF( nfields == 2 .AND. ( to_upper_case( fields(1) ) == "&END" &
                &.AND. to_upper_case( fields(2) ) == "POTENTIAL" ) ) THEN
            in_potential_section = .FALSE.
            CYCLE
        END IF

        IF( in_potential_section ) THEN
            fields(1) = to_lower_case( fields(1) )
            SELECT CASE( TRIM( fields(1) ) )
            CASE( "" )
                !!Blank line

            CASE( "pot_name" )
				write(*,*) TRIM(fields(1))
				CALL load_values(nfields, fields, field_types,keyword,sampling_param%pot_name)
				sampling_param%pot_name=to_upper_case(sampling_param%pot_name)
				therm_param%pot_name=sampling_param%pot_name
				pot_name_read = .TRUE.

			!MORSE AND DOUBLE MORSE
			CASE( "d" )
				write(*,*) TRIM(fields(1))
				CALL load_values(nfields, fields, field_types,keyword,pot_param%D)

			CASE( "alpha" )
				write(*,*) TRIM(fields(1))
				CALL load_values(nfields, fields, field_types,keyword,pot_param%alpha)
			
			CASE( "xmax" )
				write(*,*) TRIM(fields(1))
				CALL load_values(nfields, fields, field_types,keyword,pot_param%xmax)

			CASE( "asym" )
				write(*,*) TRIM(fields(1))
				CALL load_values(nfields, fields, field_types,keyword,pot_param%asym)

			CASE( "dab" )
				write(*,*) TRIM(fields(1))
				CALL load_values(nfields, fields, field_types,keyword,pot_param%dAB)

			!HARMONIC OSCILLATORS
			CASE( "omega0" )
				write(*,*) TRIM(fields(1))
				if(.not. ndim_read) call prl_abort("read_input: &
					 &'Ndim' must be specified before 'Omega0'!")
				CALL load_values(nfields, fields, field_types,keyword,pot_param%Omega0)

			CASE( "c3" )
				write(*,*) TRIM(fields(1))
				CALL load_values(nfields, fields, field_types,keyword,pot_param%c3)

			CASE( "c4" )
				write(*,*) TRIM(fields(1))
				CALL load_values(nfields, fields, field_types,keyword,pot_param%c4)

			!QUARTIC OSCILLATOR
			CASE( "qo" )
				write(*,*) TRIM(fields(1))
				CALL load_values(nfields, fields, field_types,keyword,pot_param%QO)

			CASE( "/" )		
				in_potential_section = .FALSE.

            CASE DEFAULT
                CALL prl_abort( "read_input: Unknown keyword &
                        &in section '&POTENTIAL ... &END POTENTIAL:"//TRIM( fields(1) ) )
            END SELECT
        END IF
        
	END DO

    !! Close the input file
    CLOSE( UNIT = 99 )

    !! Check reads
    IF( in_parameters_section ) CALL prl_abort( "section &PARAMETERS not closed" )
	IF( in_potential_section ) CALL prl_abort( "section &POTENTIAL not closed" )

    IF( .NOT. parameters_section_read ) CALL prl_abort( "Error! 'PARAMETERS' section not found!" )
	IF( .NOT. potential_section_read ) CALL prl_abort( "Error! 'POTENTIAL' section not found!" )

    !! Must be present inputs
    IF( .NOT. ndim_read) CALL prl_abort( "Error! &
           &'Ndim' variable not specified!" )
	IF( .NOT. nu_read) CALL prl_abort( "Error! &
           &'nu' variable not specified!" )
	IF( .NOT. pot_name_read) CALL prl_abort( "Error! &
           &'pot_name' variable not specified!" )
	IF( .NOT. temperature_read) CALL prl_abort( "Error! &
           &'temperature' variable not specified!" )
	IF( .NOT. Langevin_Nsteps_read) CALL prl_abort( "Error! &
           &'Langevin_Nsteps' variable not specified!" )
	IF( .NOT. mass_read) WRITE(*,*) "Warning: 'mass' variable not specified! &
										&Using default: mass=1."

	!ALLOCATE REAMINING ARRAYS
	allocate(sampling_param%sigp2(Ndim),therm_param%sigp2(Ndim))
	allocate(polymer%X(nu,Ndim),polymer%P(nu,Ndim))
	allocate(polymer%EigX(nu,Ndim),polymer%EigP(nu,Ndim))
	allocate(polymer%OmK(nu),polymer%EigMat(nu,nu),polymer%EigMatTr(nu,nu))
	allocate(polymer%frict(nu,Ndim))
	
	!INITIALIZE X AND P
	do i=1,nu
		polymer%X(i,:)=X_init(:)
	enddo
	polymer%P=0

	!CONVERT IN A.U.
	call convert_in_atomic_units(sampling_param,therm_param,polymer,pot_param)

	!ADJUST THERMALIZATION DAMPING IF NEEDED
	do i=1,Ndim
		if(therm_param%damping(i)<0) therm_param%damping(i)=sampling_param%damping(i)
	enddo
	!ADJUST THERMALIZATION DURATION IF NEEDED
	if(therm_param%Langevin_Nsteps==0) &
		therm_param%Langevin_Nsteps=nint(5*2*pi/(minval(therm_param%damping)*sampling_param%dt))
	!COMPUTE sigma FROM damping
	sampling_param%sigma(:)=sqrt(2*sampling_param%damping(:)*polymer%mass(:)*kb*sampling_param%temperature*polymer%nu)
	therm_param%sigma(:)=sqrt(2*therm_param%damping(:)*polymer%mass(:)*kb*sampling_param%temperature*polymer%nu)

	!COMPUTE dBeta AND sigp2
	sampling_param%dBeta=1/(kb*sampling_param%temperature*polymer%nu)
	sampling_param%sigp2(:)=(hbar**2)/(polymer%mass(:)*kb*sampling_param%temperature*polymer%nu)
	therm_param%dBeta=sampling_param%dBeta
	therm_param%sigp2=sampling_param%sigp2

	IF(IO%save_frequency<=0) IO%save_frequency=sampling_param%Langevin_Nsteps+1
	IF(IO%notify_frequency<=0) IO%notify_frequency=sampling_param%Langevin_Nsteps+1

END SUBROUTINE read_input

END MODULE m_io